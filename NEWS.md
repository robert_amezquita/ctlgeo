# ctlgeo 0.0.1

* created suite of functions for creating urls (`soft_make_urls*`), grab data (`soft_grab_data*`), along with load and parse
* first version that I'm happy creating a not-totally-dev version

# ctlgeo 0.0.0.9999

* Added a `NEWS.md` file to track changes to the package.
* Initial build with loading functions (`soft_load_data*`) and metadata parsers (`soft_parse_meta_*`) for `gse` and `gsm` levels
