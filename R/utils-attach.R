## inspiration:
## see: https://github.com/tidyverse/tidyverse/blob/master/R/attach.R
## https://github.com/tidyverse/tidyverse/blob/master/R/utils.R

.onAttach <- function(libname, pkgname) {
    packageStartupMessage(cli::rule(line = 1, width = 40))
    packageStartupMessage(
        paste('', crayon::green(cli::symbol$tick), 
              "ctlgeo", package_version('ctlgeo'), '\n',
              crayon::green(cli::symbol$info),
              "see the docs at ctlgeo.netlify.com")
    )
    packageStartupMessage(cli::rule(line = 1, width = 40))
}
